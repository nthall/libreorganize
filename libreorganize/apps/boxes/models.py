from django.db import models
from django.utils.translation import gettext_lazy as _


class Box(models.Model):
    uid = models.AutoField(primary_key=True)
    content = models.TextField()

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_boxes", "List boxes"),
            ("edit_boxes", "Edit boxes"),
            ("upload_files", "Upload files"),
        )


class Upload(models.Model):
    title = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to="files/")
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        default_permissions = ()
