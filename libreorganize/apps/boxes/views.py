from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView as GenericListView
from django.http import JsonResponse
from django.views import View
from django.shortcuts import render

from core.mixins import AccessRestrictedMixin
from apps.boxes.models import Box, Upload
from apps.boxes.forms import BoxForm, UploadForm
from core.views import LibreUpdateView, LibreDeleteView


class ListView(AccessRestrictedMixin, GenericListView):
    permissions = ("boxes.list_boxes", "boxes.edit_boxes")
    model = Box


class EditView(AccessRestrictedMixin, LibreUpdateView):
    permissions = ("boxes.edit_boxes",)
    success_url = reverse_lazy("boxes:list")
    form_class = BoxForm
    model = Box
    pk_url_kwarg = "uid"
    success_message = _("The box has been successfully edited.")


class UploadView(AccessRestrictedMixin, View):
    permissions = ("boxes.upload_boxes",)

    def get(self, request):
        upload_list = Upload.objects.all().order_by("-uploaded_at")
        return render(self.request, "boxes/upload_list.html", {"uploads": upload_list})

    def post(self, request):
        form = UploadForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            upload = form.save()
            data = {"is_valid": True, "name": upload.file.name, "url": upload.file.url}
        else:
            data = {"is_valid": False}
        return JsonResponse(data)


class DeleteUploadView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("boxes.upload_boxes",)
    superuser = True
    model = Upload
    success_url = reverse_lazy("boxes:upload")
    success_message = _("The upload file has been successfully deleted.")
