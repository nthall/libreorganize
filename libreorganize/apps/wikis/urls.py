from apps.wikis import views
from django.urls import path
from django.views.generic import RedirectView

app_name = "apps.wikis"

urlpatterns = [
    path("", RedirectView.as_view(url="/wikis/home/"), name="home"),
    path("create/", views.CreateView.as_view(), name="create"),
    path("logs/", views.LogsListView.as_view(), name="logs"),
    path("<int:uid>/logs/", views.LogsDetailView.as_view(), name="logs_detail"),
    path("<slug:url>/edit/", views.EditView.as_view(), name="edit"),
    path("<slug:url>/delete/", views.DeleteView.as_view(), name="delete"),
    path("<slug:url>/", views.WikiView.as_view(), name="view"),
]
