from django.http import Http404
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from core.mixins import AccessRestrictedMixin

from apps.wikis.models import Wiki, Log
from apps.wikis.forms import WikiForm
from core.views import LibreCreateView, LibreUpdateView, LibreDetailView, LibreDeleteView


class WikiView(AccessRestrictedMixin, LibreDetailView):
    permissions = ("wikis.view_wikis",)
    model = Wiki
    slug_field = "url"
    slug_url_kwarg = "url"
    query_pk_and_slug = True

    def get_object(self):
        result = super().get_object()
        wikis = Wiki.objects.all()
        for wiki in wikis:
            if "[[" + wiki.title + "]]" in result.content:
                result.content = result.content.replace(
                    "[[" + wiki.title + "]]", f"<a href=/wikis/{wiki.url}/>{wiki.title}</a>"
                )
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["wikis"] = Wiki.objects.exclude(url="home")
        return context


class CreateView(AccessRestrictedMixin, LibreCreateView):
    permissions = ("wikis.create_wikis",)
    model = Wiki
    form_class = WikiForm
    success_url = reverse_lazy("wikis:home")
    success_message = _("The wiki has been successfully created.")

    def get_success_url(self):
        return reverse("wikis:view", args=[self.object.url])

    def form_valid(self, form):
        result = super().form_valid(form)
        Log(author=self.request.user, wiki=self.object, content=form.cleaned_data["content"], action="Create").save()
        return result


class EditView(AccessRestrictedMixin, LibreUpdateView):
    permissions = ("wikis.edit_wikis",)
    model = Wiki
    form_class = WikiForm
    slug_field = "url"
    slug_url_kwarg = "url"
    query_pk_and_slug = True
    success_url = reverse_lazy("wikis:home")
    success_message = _("The wiki has been successfully edited.")

    def get_success_url(self):
        return reverse("wikis:view", args=[self.object.url])

    def form_valid(self, form):
        Log(author=self.request.user, wiki=self.object, content=form.cleaned_data["content"], action="Edit").save()
        return super().form_valid(form)


class DeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("wikis.delete_wikis",)
    model = Wiki
    slug_field = "url"
    slug_url_kwarg = "url"
    query_pk_and_slug = True
    success_url = reverse_lazy("wikis:home")
    success_message = _("The wiki has been successfully deleted.")

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if obj.title == "Home":
            raise Http404("No wiki matches the given query")
        return obj

    def delete(self, request, *args, **kwargs):
        wiki = self.get_object()
        Log(author=self.request.user, wiki=wiki, content=wiki.content, action="Delete").save()
        return super().delete(request, *args, **kwargs)


class LogsListView(AccessRestrictedMixin, ListView):
    permissions = ("wikis.view_wikis",)
    model = Log

    def get_queryset(self):
        qs = super().get_queryset()
        search_string = self.request.GET.get("search", None)
        if search_string is not None:
            qs = qs.filter(wiki__url=search_string)
        return qs


class LogsDetailView(AccessRestrictedMixin, LibreDetailView):
    permissions = ("wikis.view_wikis",)
    model = Log
    pk_url_kwarg = "uid"
