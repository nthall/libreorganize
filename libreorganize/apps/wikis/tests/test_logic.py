from django.test import TestCase
from apps.accounts.models import Account
from apps.wikis.models import Wiki


class TestWikisView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/wikis.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.wikis = Wiki.objects.get(uid=1)

    def test_incorrect_test(self):
        """Check delete unsuccessful (test)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/test/inexistent-test/", follow=True)
        self.assertEqual(response.status_code, 404)

    def test_invalid_url(self):
        """Test invalid URL"""
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/inexistent-test/", follow=True)
        self.assertContains(response, "The wiki doesn&#39;t exist.")
        self.assertRedirects(response, "/")


class TestWikisCreate(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/wikis.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.wikis = Wiki.objects.get(uid=1)

    def test_wikiword_link(self):
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/",
            {"url": "test", "title": "Test", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "The wiki has been successfully created.")
        response = self.client.post(
            "/wikis/test/edit/?next=/wikis/test/",
            {"title": "Test", "content": "The latest and greatest news about LibreOrganize! [[Home]]"},
            follow=True,
        )
        self.assertContains(response, "Home")

    def test_successful(self):
        """Check create successful"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/",
            {"url": "news", "title": "News", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "The wiki has been successfully created.")

    def test_invalid_title(self):
        """Invalid Title"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/",
            {"title": "News@#$%^&", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "The title must contain only URL-safe characters or spaces.")

        """More than three char in title"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/",
            {"url": "news", "title": "s", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "The title must have at least 3 characters.")

        """Test invalid titles"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/",
            {"url": "create", "title": "create", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "&quot;Create&quot; is not a valid title.")
        response = self.client.post(
            "/wikis/create/",
            {"url": "logs", "title": "logs", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "&quot;Logs&quot; is not a valid title.")

    def test_taken_title(self):
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/", {"url": "cool", "title": "Cool", "content": "This field is required."}, follow=True,
        )
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/", {"url": "cool", "title": "Cool", "content": "This field is required."}, follow=True,
        )
        self.assertContains(response, "A wiki with a similar title already exists.")

    def test_incorrect_title(self):
        """Check create unsuccessful (title)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/",
            {"url": "news", "title": "", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        self.assertContains(response, "This field is required.")

    def test_incorrect_content(self):
        """Check create unsuccessful (content)"""
        self.client.force_login(self.superuser)
        response = self.client.post("/wikis/create/", {"url": "news", "title": "News", "content": ""}, follow=True,)
        self.assertContains(response, "This field is required.")


class TestWikisEdit(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/wikis.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.wikis = Wiki.objects.get(uid=1)

    def test_successful(self):
        """Check edit successful"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/",
            {"url": "test", "title": "test", "content": "The latest and greatest news about LibreOrganize!"},
            follow=True,
        )
        response = self.client.post(f"/wikis/test/edit/", {"title": "test", "content": "Test Content"}, follow=True,)
        self.assertContains(response, "The wiki has been successfully edited.")

    def test_incorrect_wiki(self):
        """Check edit unsuccessful (wiki)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/inexistent-wiki/edit/", follow=True)
        self.assertContains(response, "The wiki doesn&#39;t exist.")

    def test_home_edit(self):
        """Check edit successful"""
        self.client.force_login(self.superuser)
        response = self.client.post(f"/wikis/home/edit/", {"title": "test", "content": "Test Content"}, follow=True,)
        self.assertContains(response, "The wiki has been successfully edited.")


class TestWikisDelete(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/wikis.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.wikis = Wiki.objects.get(uid=1)

    def test_home_delete(self):
        """Can't delete home wiki"""
        self.client.force_login(self.superuser)
        response = self.client.post("/wikis/home/delete/", follow=True)
        self.assertContains(response, "The wiki doesn&#39;t exist.")

    def test_permissions_non_superuser(self):
        """Check delete unsuccessful (permissions)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/", {"url": "news", "title": "news", "content": "This field is required."}, follow=True,
        )
        self.client.force_login(self.user)
        response = self.client.get("/wikis/news/delete/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/news/delete/", follow=True)
        self.assertContains(response, "Delete")

    def test_incorrect_wiki(self):
        """Check delete unsuccessful (wiki)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/inexistent-wiki/delete/", follow=True)
        self.assertContains(response, "The wiki doesn&#39;t exist.")

    def test_successful(self):
        """Check delete successful"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/", {"url": "news", "title": "news", "content": "This field is required."}, follow=True,
        )
        response = self.client.post("/wikis/news/delete/", follow=True)
        self.assertContains(response, "The wiki has been successfully deleted.")


class TestWikisLogs(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/wikis.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.wikis = Wiki.objects.get(uid=1)

    def test_permissions_non_superuser(self):
        """Check delete unsuccessful (permissions)"""
        self.client.force_login(self.user)
        response = self.client.get("/wikis/logs/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/logs/", follow=True)
        self.assertContains(response, "Wiki Logs")

    def test_wiki_search(self):
        """Check delete unsuccessful (permissions)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/logs/?search=home", follow=True)
        self.assertContains(response, "Wiki Logs")


class TestWikisLogsDetail(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/wikis.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.wikis = Wiki.objects.get(uid=1)

    def test_permissions_non_superuser(self):
        """Check delete unsuccessful (permissions)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/wikis/create/", {"url": "news", "title": "news", "content": "This field is required."}, follow=True,
        )
        self.client.force_login(self.user)
        response = self.client.get("/wikis/1/logs/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/1/logs/", follow=True)
        self.assertContains(response, "Home Log")

    def test_incorrect_wiki(self):
        """Check delete unsuccessful (wiki)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/wikis/999999/logs/", follow=True)
        self.assertContains(response, "The log doesn&#39;t exist.")
