from django.test import TestCase
from django.urls import resolve


class WikisUrlsCase(TestCase):
    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/wikis/create/").view_name, "wikis:create")

    def test_detail(self):
        """Check detail correct"""
        self.assertEqual(resolve("/wikis/test/").view_name, "wikis:view")

    def test_logs(self):
        """Check logs correct"""
        self.assertEqual(resolve("/wikis/logs/").view_name, "wikis:logs")

    def test_content_logs(self):
        """Check logs correct"""
        self.assertEqual(resolve("/wikis/1/logs/").view_name, "wikis:logs_detail")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/wikis/test/edit/").view_name, "wikis:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/wikis/test/delete/").view_name, "wikis:delete")
