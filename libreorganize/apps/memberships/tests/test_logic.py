from django.test import TestCase
from apps.accounts.models import Account
from apps.memberships.models import Membership
from django.core.management import call_command


class TestMembershipsList(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_successful(self):
        """Test Successful Membership"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/")
        self.assertContains(response, "2")

    def test_invalid_permissions(self):
        """Invalid Perms"""
        self.client.force_login(self.user)
        response = self.client.get("/memberships/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")


class TestMembershipsCreate(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_successful(self):
        """Test successful create Membership"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/memberships/create/", {"kind": "student", "start_date": "2020-04-01", "member": "1",}, follow=True,
        )
        self.assertContains(response, "The membership has been successfully created.")

    def test_invalid_permissions(self):
        """Invalid Perms"""
        self.client.force_login(self.user)
        response = self.client.get("/memberships/create/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")


class TestMembershipsEnroll(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_already_member(self):
        """Already a memeber"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/enroll/", follow=True)
        self.assertContains(response, "You are already a member!")
        response = self.client.post("/memberships/enroll/", follow=True)
        self.assertContains(response, "You are already a member!")

    def test_use_cash(self):
        """Use Cash to Pay for Memebership"""
        self.client.force_login(self.user)
        response = self.client.post(
            "/memberships/enroll/", {"kind": "student", "payment_method": "cash",}, follow=True,
        )
        self.assertContains(
            response,
            "Congratulations, Test! You are almost a member of LibreOrganize. An administrator will activate your membership soon.",
        )


class TestMembershipsDetail(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)


class TestMembershipsEdit(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_edit_one_year(self):
        """Test Edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/memberships/2/edit/",
            {
                "kind": "student",
                "start_date": "2020-04-01",
                "payment_method": "cash",
                "membership_period": "one_year",
                "is_active": True,
            },
            follow=True,
        )
        self.assertContains(response, "The membership has been successfully edited.")

    def test_edit_one_month(self):
        """Test Edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/memberships/2/edit/",
            {
                "kind": "student",
                "start_date": "2020-04-01",
                "payment_method": "cash",
                "membership_period": "one_month",
                "is_active": True,
            },
            follow=True,
        )
        self.assertContains(response, "The membership has been successfully edited.")

    def test_invalid_permissions(self):
        """Invalid Permissions"""
        self.client.force_login(self.user)
        response = self.client.post("/memberships/2/edit/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")


class TestMembershipsDelete(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_delete(self):
        """Delete Membership"""
        self.client.force_login(self.superuser)
        response = self.client.post("/memberships/2/delete/", follow=True)
        self.assertContains(response, "The membership has been deleted.")


class TestRenewalListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)


class TestCommands(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_edit_one_year(self):
        """Test Edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/memberships/2/edit/",
            {
                "kind": "student",
                "start_date": "2016-04-01",
                "payment_method": "cash",
                "membership_period": "one_year",
                "is_active": True,
            },
            follow=True,
        )
        self.assertContains(response, "The membership has been successfully edited.")
        response = self.client.get("/memberships/2/", follow=True)
        self.assertContains(response, "Apr 01, 2017")
        self.assertContains(response, "Yes")
        call_command("update_membership_status")
        response = self.client.get("/memberships/2/", follow=True)
        self.assertContains(response, "No")
