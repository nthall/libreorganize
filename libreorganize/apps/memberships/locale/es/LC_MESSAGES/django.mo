��    #      4  /   L        L   	     V  0   ]     �  �   �  Y   2     �  A   �     �     �     �               "     2     9  H   E     �     �     �     �     �  
   �     �      �  -     ,   B  ;   o  9   �     �                    '     A  \   b     �  5   �     �  �   	  X   �	     �	  9   
     =
     Q
     Z
     n
     �
     �
     �
  
   �
  V   �
            %         F     Z     j     z      �  (   �  +   �  /   �  7   '     _     }     �     �     �                                  	                "         !                                     #                  
                                           A member who is paying by cash has joined! Login to activate his membership. Active Are you sure you want to delete this membership? CREATE MEMBERSHIP Congratulations, {request.user.first_name}! You are almost a member of {settings.SITE_NAME}. An administrator will activate your membership soon. Congratulations, {request.user.first_name}! You are now a member of {settings.SITE_NAME}. Create Membership Creating the membership assumes that the member has paid the fee. DELETE MEMBERSHIP Delete Delete Membership EDIT MEMBERSHIP ENROLL Edit Membership Enroll MEMBERSHIPS Manually created memberships can only have cash as their payment method! Member Memberships Must be formatted as YYYY-MM-DD New Member | %s  Payment Method Start Date Submit The membership has been deleted. The membership has been successfully created. The membership has been successfully edited. There is already a membership associated with this account. There will be no more charges related to this membership. This action is irreversible. Type VIEW MEMBERSHIP View Membership You are already a member! Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-06 23:58-0400
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
Language-Team: 
X-Generator: Poedit 2.3
 ¡Se ha unido un miembro que ha paga en efectivo! Inicie sesión para activar su membresía. Activo ¿Está seguro de que desea eliminar esta membresía? CREAR MEMBRESÍA ¡Felicitaciones, {request.user.first_name}! Eres casi un miembro de {settings.SITE_NAME}. Un administrador activará su membresía pronto. ¡Felicitaciones, {request.user.first_name}! Ahora eres miembro de {settings.SITE_NAME}. Crear Membresía Al crear la membresía asume que el miembro ya ha pagado. ELIMINAR MEMBRESÍA Eliminar Eliminar Membresía EDITAR MEMBRESÍA INSCRIBIRSE Editar Membresía Inscribirse MEMBERSHIP ¡Las membresías creadas manualmente solo pueden tener efectivo como método de pago! Miembro Membresías Debe estar formateada como AAAA-MM-DD Nuevo miembro | %s  Método de pago Fecha de inicio Enviar La membresía ha sido eliminada. La membresía se ha creado exitosamente. La membresía ha sido editada exitosamente. Ya hay una membresía asociada con esta cuenta. No habrá más cargos relacionados con esta membresía. Esta acción es irreversible. Tipo VER MEMBRESÍA Ver Membresía Ya eres miembro! 