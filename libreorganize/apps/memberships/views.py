import datetime

from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView as GenericListView

from core.mixins import AccessRestrictedMixin
from core.views import LibreDeleteView, LibreCreateView, LibreUpdateView, LibreDetailView
from apps.memberships.models import Membership, MembershipLog
from apps.memberships.forms import EnrollForm, CreateForm, EditForm


class ListView(AccessRestrictedMixin, GenericListView):
    permissions = "memberships.list_memberships"
    model = Membership


class RenewalListView(AccessRestrictedMixin, GenericListView):
    permissions = "memberships.view_memberships"
    model = MembershipLog


class DetailView(AccessRestrictedMixin, LibreDetailView):
    permissions = ("memberships.view_memberships",)
    personal = True
    model = Membership
    pk_url_kwarg = "uid"


class DeleteView(AccessRestrictedMixin, LibreDeleteView):
    permissions = ("memberships.delete_memberships",)
    personal = True
    model = Membership
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("memberships:list")
    success_message = _("The membership has been deleted.")


class EditView(AccessRestrictedMixin, LibreUpdateView):
    permissions = ("memberships.edit_memberships",)
    model = Membership
    form_class = EditForm
    pk_url_kwarg = "uid"
    success_url = reverse_lazy("memberships:list")
    success_message = _("The membership has been successfully edited.")


class CreateView(AccessRestrictedMixin, LibreCreateView):
    permissions = ("memberships.create_memberships",)
    model = Membership
    form_class = CreateForm
    success_url = reverse_lazy("memberships:list")
    success_message = _("The membership has been successfully created.")


class EnrollView(AccessRestrictedMixin, LibreCreateView):
    model = Membership
    form_class = EnrollForm
    success_url = "/"

    def get_success_message(self, cleaned_data):
        return f"Congratulations, {self.request.user.first_name}! You are almost a member of {settings.SITE_NAME}. An administrator will activate your membership soon."

    def dispatch(self, request, *args, **kwargs):
        if hasattr(request.user, "membership"):
            messages.add_message(request, messages.ERROR, _("You are already a member!"))
            return HttpResponseRedirect(self.success_url)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        # Fill in some default values
        form.instance.start_date = datetime.date.today()
        form.instance.member = self.request.user
        result = super().form_valid(form)

        # Send an email to the administrator
        url = self.request.build_absolute_uri(f"/memberships/{form.instance.uid}/")
        html = render_to_string(
            "email.html",
            {
                "url": url,
                "message": _(
                    "A member who is paying by cash has joined! Login to activate his membership. Edit Membership and make the Account Active once you have recieved the money."
                ),
                "button": "View Membership",
            },
        )
        text = strip_tags(html).replace("View Membership", url)
        send_mail(
            subject=_("New Member | %s ") % settings.SITE_NAME,
            message=text,
            html_message=html,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=(settings.NOTIFICATIONS_EMAIL,),
            fail_silently=True,
        )

        return result
