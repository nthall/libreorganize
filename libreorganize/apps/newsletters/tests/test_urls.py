from django.test import TestCase
from django.urls import resolve


class NewsletterUrls(TestCase):
    def test_list(self):
        """Test newsletter list"""
        self.assertEqual(resolve("/newsletters/").view_name, "newsletters:list")

    def test_create(self):
        """Test newsletter create"""
        self.assertEqual(resolve("/newsletters/create/").view_name, "newsletters:create")

    def test_edit(self):
        """Test newsletter edit"""
        self.assertEqual(resolve("/newsletters/1/edit/").view_name, "newsletters:edit")

    def test_delete(self):
        """Test newsletter edit"""
        self.assertEqual(resolve("/newsletters/1/delete/").view_name, "newsletters:delete")
