from django.test import TestCase

from apps.newsletters.models import SignUp
from apps.accounts.models import Account


class TestNewsletterList(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_list(self):
        """Test list"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        response = self.client.get("/newsletters/", follow=True)
        self.assertContains(response, "Joe Smith")


class TestNewsletterCreate(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_create(self):
        """Test create"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The newsletter has been successfully created.")

    def test_invalid_name(self):
        """Test invalid full name"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/newsletters/create/",
            {"full_name": "^#*$*%#", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "Enter a valid full name.")

    def test_template(self):
        """Check create template"""
        self.client.force_login(self.superuser)
        response = self.client.post("/newsletters/create/", follow=True)
        self.assertTemplateUsed(response, template_name="newsletters/signup_create.html")


class TestNewsletterEdit(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_edit(self):
        """Test edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The newsletter has been successfully created.")
        response = self.client.post(
            "/newsletters/1/edit/?next=/newsletters/",
            {"full_name": "Joe Smith Edit", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The email has been successfully edited.")


class TestNewsletterDelete(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_edit(self):
        """Test delete"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/newsletters/create/",
            {"full_name": "Joe Smith", "email": "joe.smith@gmail.com", "language": "en"},
            follow=True,
        )
        self.assertContains(response, "The newsletter has been successfully created.")
        response = self.client.post("/newsletters/1/delete/?next=/newsletters/", follow=True,)
        self.assertContains(response, "The email has been successfully deleted.")
