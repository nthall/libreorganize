��    "      ,  /   <      �  +   �     %     .     ;     D  
   M     X     e     r     y     �  
   �     �  
   �     �     �     �     �  %   �            
   )     4  0   ;     l  (   �  '   �  >   �  -        F     c     i     y     �  +   �  
   �     �  
   �  
     
             )     9     B     R     _     m     u     �     �  
   �      �  ,   �     �     �          &  E   -     s  *   �  ,   �  ;   �  0   $	     U	     s	     z	     �	                	                   
                               !                                                     "                                       Are you sure you want to delete this event? CALENDAR CREATE EVENT Calendar Check in Checked in Create Event DELETE EVENT Delete Delete Event Description EDIT EVENT EVENTS Edit Event End Date Events Location Maximum No. Participants Must be formatted as YYYY-MM-DD HH:MM Participants Should be an address Start Date Submit The end date cannot be less than the start date. The event has been deleted. The event has been successfully created. The event has been successfully edited. The maximum number of participants needs to be higher than -1. There are no more empty seats for this event! This action is irreversible. Title You checked in. You have already checked in! Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-06 23:57-0400
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
Language-Team: 
X-Generator: Poedit 2.3
 ¿Seguro qué quieres eliminar este evento? CALENDARIO CREAR EVENTO Calendario Registrado Registrado Crear Evento ELIMINAR EVENTO Eliminar Eliminar Evento Descripción EDITAR EVENTO EVENTOS Editar Evento Fecha final Eventos Ubicación Número máximo de participantes Debe estar formateado como AAAA-MM-DD HH: MM Participantes Debe ser una dirección Fecha de inicio Enviar La fecha de finalización no puede ser inferior a la fecha de inicio. El evento ha sido cancelado. El evento ha sido creado con exitosamente. El evento ha sido editado con éxitosamente. El número máximo de participantes debe ser superior a -1. ¡No hay más asientos vacíos para este evento! Esta acción es irreversible. Titulo Te registraste. ¡Ya te has registrado! 