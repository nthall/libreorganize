from django.test import TestCase
from apps.events.apps import EventsConfig


class EventsAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(EventsConfig.name, "apps.events")
