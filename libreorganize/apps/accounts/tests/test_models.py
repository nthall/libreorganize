import shutil
from django.test import TestCase
from django.conf import settings
from apps.accounts.models import Account


class AccountsModelsTestCase(TestCase):
    fixtures = ["tests/test_data/accounts.json"]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)

    def test_avatar_url(self):
        """Check avatar_url correct"""
        self.assertEqual(self.user.avatar_url, "/media/core/static/img/logos/libreorganize.png")
        self.assertEqual(self.superuser.avatar_url, "/static/img/profile_avatar.png")

    def test_phone_number(self):
        """Check phone_number correct"""
        self.assertEqual(self.user.phone_number, "(703) 998-3934")
        self.assertEqual(self.superuser.phone_number, "+1 (703) 998-3934")
        self.user.phone = "123456789"
        self.assertEqual(self.user.phone_number, "123456789")

    def test_str(self):
        """Check __str__ correct"""
        self.assertEqual(self.user.__str__(), "Test User")
        self.assertEqual(self.superuser.__str__(), "Test Superuser")

    def test_get_full_name(self):
        """Check get_full_name correct"""
        self.assertEqual(self.user.get_full_name(), "Test User")
        self.assertEqual(self.superuser.get_full_name(), "Test Superuser")

    def test_get_short_name(self):
        """Check get_short_name correct"""
        self.assertEqual(self.user.get_short_name(), "Test U.")
        self.assertEqual(self.superuser.get_short_name(), "Test S.")

    def test_get_full_address(self):
        """Check get_full_address correct"""
        self.assertEqual(
            self.user.get_full_address(), "5041 7th Rd S, #T1, Arlington, Virginia 22204, United States of America"
        )
        self.assertEqual(self.superuser.get_full_address(), "")

    def test_is_superuser(self):
        """Check is_super_user correct"""
        self.assertEqual(self.user.is_superuser, False)
        self.assertEqual(self.superuser.is_superuser, True)
