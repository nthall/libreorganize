import numpy as np
import operator
from random import randint


class Election(object):
    def __init__(self, candidates):
        self.candidates = np.array(candidates)
        self.votes = []
        self.candidates_left = self.candidates

    def vote(self, ballot):
        if type(ballot) == list:
            vote = self.read_vote_from_list(ballot)

        self.votes.append(vote)

    def read_vote_from_list(self, ballot):
        ballot_set = set(ballot)
        if len(ballot_set) != len(ballot):
            raise ValueError("Voter has voted for the same candidate")

        if len(ballot) > len(self.candidates):
            raise ValueError("Voter has voted too many times")

        return ballot

    def count_first_ranked(self, votes):
        # Determine Who is in First
        first_ranks = np.array([vote[0] for vote in votes if len(vote) > 0])

        # Create Dictionary with # of Votes for Each Candidate
        frequencies = dict()
        for candidate in self.candidates_left:
            frequencies[candidate] = np.sum(first_ranks == candidate)

        return frequencies

    def has_winner(self, counts):
        values = np.fromiter(counts.values(), dtype=np.int)

        is_unique = np.sum(values == values.max()) == 1
        is_absolute = 2 * values.max() > values.sum()

        return is_unique and is_absolute

    def get_winner(self, counts):
        if not self.has_winner(counts):
            raise Exception("No winner of first rank votes")

        values = np.array([counts[candidate] for candidate in self.candidates_left], dtype=np.int)
        return self.candidates_left[values.argmax()]

    def get_candidates_to_eliminate(self, counts, rule="all"):
        values = np.array([counts[c] for c in self.candidates_left], dtype=np.int)

        return self.candidates_left[values == values.min()]

    def get_candidate_id(self, candidate):
        return np.where(self.candidates == candidate)[0][0]

    def remove_candidate_from_vote(self, vote, candidate_id):
        if candidate_id in vote:
            vote.remove(candidate_id)
        return vote

    def eliminate_candidate(self, votes, candidate):
        candidate_id = self.get_candidate_id(candidate)
        return [self.remove_candidate_from_vote(vote, candidate_id) for vote in votes]

    def tally(self):
        votes = self.votes
        winner = False

        # Break Loop Once Winner Has Been Found
        for n_round in range(len(self.candidates)):
            frequencies = self.count_first_ranked(votes)

            if self.has_winner(frequencies):
                winner = self.get_winner(frequencies)
                break

            candidates = self.get_candidates_to_eliminate(frequencies)
            # Tests if Tie Occurs and Flips a Coin
            if len(candidates) == len(self.candidates_left):
                coinflip = randint(0, len(candidates) - 1)
                winner = candidates[coinflip]
                return winner

            for candidate in candidates:
                votes = self.eliminate_candidate(votes, candidate)
                self.candidates_left = self.candidates_left[self.candidates_left != candidate]

        return winner
