from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import DeleteView, DetailView, UpdateView, CreateView

from core.mixins import Custom404Mixin, SuccessUrlMixin


class LibreDeleteView(SuccessUrlMixin, Custom404Mixin, DeleteView):
    success_message = None

    def delete(self, request, *args, **kwargs):
        # delete doesn't support automatic success messages, so we have to do this the hard way
        messages.success(self.request, self.success_message)
        return super(DeleteView, self).delete(request, *args, **kwargs)


class LibreDetailView(Custom404Mixin, DetailView):
    # This serves as the base class for all LibreDetailViews.  It provides useful behavior (e.g., 404 redirects to /)
    pass


class LibreUpdateView(SuccessUrlMixin, Custom404Mixin, SuccessMessageMixin, UpdateView):
    # This serves as the base class for all Edit/Update Views.  It provides useful behavior (e.g., 404 redirects to /)
    pass


class LibreCreateView(SuccessMessageMixin, CreateView):
    # This serves as the base class for all Create Views.  It provides useful behavior (e.g., 404 redirects to /)
    pass
