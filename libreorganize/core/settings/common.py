import os
import socket
from django.contrib.messages import constants as message_constants
from django.utils.translation import gettext_lazy as _

# ---------------------------------------------------------------------
# CORE
# ---------------------------------------------------------------------

FQDN = socket.getfqdn()
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
SECRET_KEY = os.environ["LO_SECRET_KEY"]
ALLOWED_HOSTS = ["*"]
DEBUG = True
WSGI_APPLICATION = "core.wsgi.application"
ROOT_URLCONF = "core.default_urls"
AUTH_USER_MODEL = "accounts.Account"
SITE_NAME = "LibreOrganize"
DOMAIN = os.environ["LO_DOMAIN"]

# ---------------------------------------------------------------------
# INTERNATIONALIZATION
# ---------------------------------------------------------------------

LANGUAGE_CODE = "en"
TIME_ZONE = "America/New_York"
DATE_FORMAT = "M d, Y"
DATETIME_FORMAT = "M d, Y, g:i A"

USE_I18N = True
USE_L10N = False
USE_TZ = False

# ---------------------------------------------------------------------
# EVENTS
# ---------------------------------------------------------------------

FIRST_WEEKDAY = 6
ICAL_PRODID = f"-//{FQDN}//LibreOrganize Calendar//{LANGUAGE_CODE.upper()}"
ICAL_FILENAME = f"{os.environ['LO_DB_NAME']}.ics"

# ---------------------------------------------------------------------
# DATABASE
# ---------------------------------------------------------------------

if DEBUG:
    DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "libreorganize.sqlite3"}}
else:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "NAME": os.environ["LO_DB_NAME"],
            "USER": os.environ["LO_DB_USER"],
            "PASSWORD": os.environ["LO_DB_PASSWORD"],
            "HOST": os.environ.get("LO_DB_HOST", "localhost"),
            "PORT": os.environ.get("LO_DB_PORT", ""),
        }
    }

# ---------------------------------------------------------------------
# EMAIL
# ---------------------------------------------------------------------

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
NOTIFICATIONS_EMAIL = "admin@novawebdevelopment.org"
EMAIL_HOST = os.environ["LO_EMAIL_HOST"]
EMAIL_USE_TLS = True
EMAIL_PORT = 587
EMAIL_HOST_USER = os.environ["LO_EMAIL_HOST_USER"]
EMAIL_HOST_PASSWORD = os.environ["LO_EMAIL_HOST_PASSWORD"]

# ---------------------------------------------------------------------
# CRISPY FORMS
# ---------------------------------------------------------------------

CRISPY_TEMPLATE_PACK = "bootstrap4"
MESSAGE_TAGS = {
    message_constants.DEBUG: "debug",
    message_constants.INFO: "info",
    message_constants.SUCCESS: "success",
    message_constants.WARNING: "warning",
    message_constants.ERROR: "danger",
}

# ---------------------------------------------------------------------
# MISCELLANEOUS
# ---------------------------------------------------------------------

NAME_REGEX = r"^[A-Za-zÁÉÍÓÚÑÜáéíóúñü\.\,\'\-\ ]+$"
PASSWORD_REGEX = r"^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z]).*$"
PASSWORD_RESET_TIMEOUT_DAYS = 1

# ---------------------------------------------------------------------
# PACKAGES
# ---------------------------------------------------------------------

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # Third-Party Apps
    "django_countries",
    "crispy_forms",
    "tempus_dominus",
    "django_ckeditor_5",
    "social_django",
    # LibreOrganize Apps
    "core",
    "apps.accounts",
    "apps.boxes",
    "apps.events",
    "apps.memberships",
    "apps.wikis",
    "apps.polls",
    "apps.newsletters",
    "django_cleanup.apps.CleanupConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

# ---------------------------------------------------------------------
# TEMPLATES
# ---------------------------------------------------------------------

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]
# ---------------------------------------------------------------------
# OAUTH GOOGLE AUTHENTICATION REQUIREMENTS
# ---------------------------------------------------------------------

AUTHENTICATION_BACKENDS = (
    "social_core.backends.google.GoogleOAuth2",
    "django.contrib.auth.backends.ModelBackend",
)

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.environ["LO_GOOGLE_OAUTH2_KEY"]
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.environ["LO_GOOGLE_OAUTH2_SECRET"]

LOGIN_URL = "/authentication/login/google-oauth2/"
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"
SOCIAL_AUTH_URL_NAMESPACE = "authentication"

# ---------------------------------------------------------------------
# STATIC AND MEDIA FILES
# ---------------------------------------------------------------------

STATIC_URL = "/static/"
MEDIA_URL = "/media/"

if DEBUG:
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
else:
    STATIC_ROOT = "/var/www/libreorganize/static/"
    MEDIA_ROOT = "/var/www/libreorganize/media/"

# ---------------------------------------------------------------------
#  CHOICES
# ---------------------------------------------------------------------

TITLE_CHOICES = (
    ("", ""),
    ("mr", _("Mr.")),
    ("mx", _("Mx.")),
    ("mrs", _("Mrs.")),
    ("ms", _("Ms.")),
    ("miss", _("Miss")),
    ("dr", _("Dr.")),
    ("prof", _("Prof.")),
)

GENDER_CHOICES = (
    ("", ""),
    ("male", _("Male")),
    ("female", _("Female")),
    ("other", _("Other")),
)

FREQUENCY_CHOICES = (
    ("weekly", "Weekly"),
    ("monthly", "Monthly"),
    ("yearly", "Yearly"),
)

LANGUAGES = (("en", _("English")), ("es", _("Spanish")))

MEMBERSHIP_TYPES = (("student", _("Student - $5.00")), ("normal", _("Normal - $10.00")))

MEMBERSHIP_PERIOD = (("one_year", _("One Year")), ("one_month", _("One Month")))

PAYMENT_METHODS = (("cash", _("Cash")),)

TYPE_VOTING = (("ranked", _("Ranked Choice")), ("normal", _("Regular Voting")))

if os.path.exists(os.path.join(BASE_DIR, "theme")):
    from theme.settings import *

    LOCALE_PATHS = (os.path.join(BASE_DIR, "theme", "locale"),)
    STATICFILES_DIRS = (os.path.join(BASE_DIR, "theme", "static"),)
    TEMPLATES = [
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": [os.path.join(BASE_DIR, "theme", "templates")],
            "APP_DIRS": True,
            "OPTIONS": {
                "context_processors": [
                    "django.template.context_processors.debug",
                    "django.template.context_processors.request",
                    "django.contrib.auth.context_processors.auth",
                    "django.contrib.messages.context_processors.messages",
                ],
            },
        },
    ]

TEMPLATES[0]["OPTIONS"]["context_processors"].append("apps.events.context_processors.events_processor")

# ---------------------------------------------------------------------
#  CKEditor5
# ---------------------------------------------------------------------

CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_5_CONFIGS = {
    "box": {
        "toolbar": [
            "fontFamily",
            "heading",
            "|",
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "|",
            "alignment",
            "horizontalLine",
            "|",
            "bulletedList",
            "numberedList",
            "|",
            "insertTable",
            "imageUpload",
            "link",
            "blockQuote",
            "code",
            "|",
            "undo",
            "redo",
        ],
        "alignment": {"options": ["left", "center", "right"]},
        "image": {
            "toolbar": [
                "imageStyle:alignLeft",
                "imageStyle:full",
                "imageStyle:alignRight",
                "imageStyle:alignCenter",
                "imageStyle:side",
            ],
            "styles": ["full", "side", "alignLeft", "alignRight", "alignCenter",],
        },
        "table": {
            "contentToolbar": ["tableColumn", "tableRow", "mergeTableCells", "tableProperties", "tableCellProperties"]
        },
        "heading": {
            "options": [
                {"model": "paragraph", "title": "Paragraph", "class": "ck-heading_paragraph"},
                {"model": "heading1", "view": "h1", "title": "Heading 1", "class": "ck-heading_heading1"},
                {"model": "heading2", "view": "h2", "title": "Heading 2", "class": "ck-heading_heading2"},
                {"model": "heading3", "view": "h3", "title": "Heading 3", "class": "ck-heading_heading3"},
            ]
        },
        "basicEntities": False,
        "entities": False,
    },
    "wiki": {
        "toolbar": [
            "fontFamily",
            "heading",
            "fontColor",
            "|",
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "subscript",
            "superscript",
            "|",
            "horizontalLine",
            "alignment",
            "outdent",
            "indent",
            "|",
            "bulletedList",
            "numberedList",
            "|",
            "insertTable",
            "imageUpload",
            "link",
            "blockQuote",
            "code",
            "|",
            "undo",
            "redo",
        ],
        "alignment": {"options": ["left", "center", "right"]},
        "image": {
            "toolbar": [
                "imageStyle:alignLeft",
                "imageStyle:full",
                "imageStyle:alignRight",
                "imageStyle:alignCenter",
                "imageStyle:side",
            ],
            "styles": ["full", "side", "alignLeft", "alignRight", "alignCenter",],
        },
        "table": {
            "contentToolbar": ["tableColumn", "tableRow", "mergeTableCells", "tableProperties", "tableCellProperties"]
        },
        "heading": {
            "options": [
                {"model": "paragraph", "title": "Paragraph", "class": "ck-heading_paragraph"},
                {"model": "heading1", "view": "h1", "title": "Heading 1", "class": "ck-heading_heading1"},
                {"model": "heading2", "view": "h2", "title": "Heading 2", "class": "ck-heading_heading2"},
                {"model": "heading3", "view": "h3", "title": "Heading 3", "class": "ck-heading_heading3"},
            ]
        },
        "basicEntities": False,
        "entities": False,
    },
}

# special field names
TOTAL_FORM_COUNT = "TOTAL_FORMS"
INITIAL_FORM_COUNT = "INITIAL_FORMS"
MIN_NUM_FORM_COUNT = "MIN_NUM_FORMS"
MAX_NUM_FORM_COUNT = "MAX_NUM_FORMS"
ORDERING_FIELD_NAME = "ORDER"
DELETION_FIELD_NAME = "DELETE"

# default minimum number of forms in a formset
DEFAULT_MIN_NUM = 1

# default maximum number of forms in a formset, to prevent memory exhaustion
DEFAULT_MAX_NUM = 10
