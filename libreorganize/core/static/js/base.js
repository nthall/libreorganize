$(document).scroll(function () {
    $(".navbar").toggleClass("scrolled", $(this).scrollTop() > 0);
});

$(document).ready(function () {
    Array.from(document.querySelectorAll("a")).filter(l => l.hostname !== location.hostname).forEach(el => el.addEventListener("click", evtL));
    function evtL(e) {
        e.preventDefault();
        var redirectLocation = this.href;
        $("#externalRedirect").modal();
        $('#confirmExternalRedirect').on('click', function () {
            window.location.href = redirectLocation;
            return;
        });
    }
    setTimeout(function () {
        $(".alert-dismissible").alert("close");
    }, 5000);
    $.fn.dataTable.moment("MMM D, YYYY, H:mm A");
    table = $('#jsTable').DataTable({
        responsive: true,
        dom: '<"row no-gutters flex-nowrap"<"flex-grow-1"f><"flex-shrink-0 ml-1"l>>t<"row"<"col-sm-12 col-md-6"B><"col-sm-12 col-md-6"p>><"row"<"col-sm-12 col-md-4"i>>',
        buttons: [
            'pdf'
        ],
        lengthMenu: [5, 10, 25, 50, 100],
        pagingType: "numbers",
        pageLength: 10,
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Search"
        },
        columnDefs: [{
            targets: [-1],
            orderable: false
        },
        {
            targets: [0, -1],
            responsivePriority: 1
        }]
    });
    $('#jsTable_length').removeClass('dataTables_length');
    $('#jsTable_filter').removeClass('dataTables_filter');
    $('#jsTable_filter > label').contents().unwrap();
    $('#jsTable_length > label').contents().unwrap();
    $('#jsTable_filter > input').removeClass("form-control-sm");
    $('#jsTable_length > select').removeClass("custom-select-sm form-control-sm");
});

$(function () {
    $("#open-dashboard").click(function () {
        document.getElementById("dashboard").classList.add("visible");
        $("body").css("overflow", "hidden");
    });
    $("#close-dashboard").click(function () {
        document.getElementById("dashboard").classList.remove("visible");
        $("body").css("overflow", "auto");
    });
});

function genPDF() {
    $(".image").css({ "width": "100px", "align": "left" });
    $(".image").children().css({ "width": "100px", "align": "left" });
    var margin = {
        top: 15,
        left: 10,
        right: 0,
        bottom: 15
    };

    var doc = new jsPDF("portrait", "mm", "letter");
    doc.fromHTML($('#pdf').get(0), 15, 15,
        { 'width': 175 },
        function (bla) { doc.save('wiki.pdf'); location.reload(); },
        margin);
}