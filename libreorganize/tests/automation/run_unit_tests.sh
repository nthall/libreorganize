#!/bin/bash

set -e

COVERAGE=coverage
hash coverage 2>/dev/null || { COVERAGE=coverage.exe; }

echo -e "\033[1;31mRunning Unit Tests with Coverage...\033[0m"
$COVERAGE run --source='.' --omit='*/core/*','local_settings.py','local_urls.py','manage.py','*/tests/*','*/migrations/*' manage.py test

$COVERAGE report -m
$COVERAGE erase
